<?php 
class notes_controller{
	function __construct($action){
		$SiteBody = TemplateMenuBuilder::getInstans();
		switch ($action) {
			case 'addnoteaction':
				$SiteBody->setTemplateTextMain($this->addnotesAction());
			break;
			case 'sendnoteaction':
				$SiteBody->setTemplateTextMain($this->sendnoteAction());
			break;
			case 'showaction':
				$SiteBody->setTemplateTextMain($this->showAction());
				if ($_SESSION['USERDATA_ACCESS'][2]) {
					$SiteBody->setTemplateLeftAdminMenu('LeftAdminMenu');
				}
			break;
			case 'deletenoteaction':
				$SiteBody->setTemplateTextMain($this->deletenoteAction());
			break;
			case 'regionaction':
				$SiteBody->setTemplateTextMain($this->defaultAction('region'));
			break;
			case 'subregionaction':
				$SiteBody->setTemplateTextMain($this->defaultAction('subregion'));
			break;
			case 'usernotesaction':
				$SiteBody->setTemplateTextMain($this->defaultAction('usernotes'));
			break;
			case 'mynotesaction':
				if ($_SESSION['USERDATA_id']==0) FrontController::GenerateHeaderLocation("200 HTTP OK", "/?page=notes");
				$SiteBody->setTemplateTextMain($this->defaultAction('mynotes'));
				break;
			default:
				$SiteBody->setTemplateTextMain($this->defaultAction());
			break;
		}
		
		$SiteBody->setTemplateLeftTopMenu('LeftTopMenu');
		}
	/**
	 * Действие по умолчанию,отображается сам сайт
	 */
	private function defaultAction($filter='none'){
		$FC = FrontController::getInstans();
		$NameModel = $FC->GetCurrentPage().'_model';
		$IncludePathModel = 'application/models/'.$NameModel.'.php';

		if (file_exists($IncludePathModel)){
			require_once $IncludePathModel;
			if (class_exists($NameModel)){
				
				if ($filter!='none') {
					$DataFilter[] = $filter;
					$DataFilter[] = htmlspecialchars($FC->GetRequestUserParam('id'));
				} 
				
				$View = new $NameModel();
				return $View->RenderView($FC->GetCurrentPage(),$DataFilter);
				
			} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindClassModels.$NameModel.':'.$IncludePathModel);
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindModels.$IncludePathModel);	
	}
	
	private function addnotesAction(){
		$FC = FrontController::getInstans();
		$NameModel = $FC->GetCurrentPage().'_addnote_model';
		$IncludePathModel = 'application/models/'.$NameModel.'.php';

		if (file_exists($IncludePathModel)){
			require_once $IncludePathModel;
			if (class_exists($NameModel)){
	
				$View = new $NameModel();
				return $View->RenderView($FC->GetCurrentPage());
				
			} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindClassModels.$NameModel.':'.$IncludePathModel);
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindModels.$IncludePathModel);	
	}
	
	private function showAction(){
		$FC = FrontController::getInstans();
		$NameModel = $FC->GetCurrentPage().'_show_model';
		$IncludePathModel = 'application/models/'.$NameModel.'.php';

		if (file_exists($IncludePathModel)){
			require_once $IncludePathModel;
			if (class_exists($NameModel)){
	
				$View = new $NameModel();
				return $View->RenderView($FC->GetCurrentPage());
				
			} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindClassModels.$NameModel.':'.$IncludePathModel);
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindModels.$IncludePathModel);	
	}
	
	private function deletenoteAction(){
		$FC = FrontController::getInstans();
		$NameModel = $FC->GetCurrentPage().'_deletenote_model';
		$IncludePathModel = 'application/models/'.$NameModel.'.php';
	
		if (file_exists($IncludePathModel)){
			require_once $IncludePathModel;
			if (class_exists($NameModel)){
	
				$View = new $NameModel();
				return $View->RenderView($FC->GetCurrentPage());
	
			} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindClassModels.$NameModel.':'.$IncludePathModel);
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindModels.$IncludePathModel);
	}
	
	private function sendnoteAction(){
		$FC = FrontController::getInstans();
		$NameModel = $FC->GetCurrentPage().'_sendnote_model';
		$IncludePathModel = 'application/models/'.$NameModel.'.php';

		if (file_exists($IncludePathModel)){
			require_once $IncludePathModel;
			if (class_exists($NameModel)){
	
				$View = new $NameModel();
				return $View->RenderView($FC->GetCurrentPage());
				
			} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindClassModels.$NameModel.':'.$IncludePathModel);
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindModels.$IncludePathModel);	
	}
}