<?php
/**
 * <b>Главный разруливатель</b>
 *  Через этот клас проходят все запросы. Определяет что запускать
 * @author PlayLinsor
 * @category core
 * @version 1.0.1n
 * @copyright 22.01.14
 */
class FrontController {
	private static $ClassHandle = null;
	protected $CurrentPage,$controller,$action,$params;

	/**
	 * Конструктор извлекает из URL все параметры и задает название контроллерам и действиям
	 */
	private function __construct(){
		
		if ($_GET['page']!=null) {
			$RequestNameController = trim(htmlspecialchars($_GET['page']));
			$this->controller = $RequestNameController.'_controller';
		}else $this->controller = 'index_controller';
		
		if ($_GET['action']!=null) {
			$RequestNameAction = trim(htmlspecialchars($_GET['action']));
			$this->action = $RequestNameAction.'action';
		}else $this->action = 'indexaction';
		
		// extract varibles
		$request = $_SERVER['REQUEST_URI'];
		$requestParametrs = explode('?', trim($request));
		$requestParametrsSplits = explode('&', $requestParametrs[1]);
		
		if ($requestParametrsSplits[0]!=null){
			$UserDataArray = array();
			for ($i=0,$cn = count($requestParametrsSplits);$i<$cn;$i++){	
				$RequestUserData = explode('=', $requestParametrsSplits[$i]);
				if ($RequestUserData[0]=='page' || $RequestUserData[0]=='action')
					continue;
				$UserDataArray[$RequestUserData[0]] = $RequestUserData[1];
			}
			$this->params = $UserDataArray;
		}
		$this->CurrentPage = $RequestNameController;
	}
	
	/**
	 * Соосно сама функция распределения, и передача управления к конкретному контроллеру
	 */
	public function route(){
		$IncludePathController = 'application/controllers/'.strtolower($this->controller).'.php';
		if (file_exists($IncludePathController)){
			require_once $IncludePathController;
			if (class_exists($this->controller))
				$LoadController = new $this->controller(strtolower($this->action));	
			else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindClassController.$this->action);	
		}
		else $this->GeneratePage404(); 
	}
	
	/**
	 * Начальная загрузка
	 */
	static public function getInstans(){
		if (self::$ClassHandle==null)
			self::$ClassHandle = new self();
		return self::$ClassHandle;
	}
	/**
	 * Защита от дурака
	 */
	Private function __clone(){
	}
	
	/**
	 * Переадресация на страницу ошибки(404)
	 */
	public function GeneratePage404(){
		FrontController::GenerateHeaderLocation('404 Not Found', '?page=error');
	}
	/**
	 * Заголовок о переадресации
	 */
	public static function GenerateHeaderLocation($status,$in){
		$host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 '.$status);
		header('Location:'.$host.$in);
	}
	
	/**
	 * Возвращает текущую страницу переданную в $_GET['page']
	 * @return string Page
	 */
	public function GetCurrentPage(){
		if ($this->CurrentPage!=null) return strtolower($this->CurrentPage);
			else $CurrentPageWithController = explode('_', strtolower($this->controller));
		return $CurrentPageWithController[0];
	}
	
	public function GetRequestUserDataList(){
		return $this->params;
	}
	
	public function GetRequestUserParam($Param){
		return $this->params[$Param];
	}
	
	public function GetAction(){
		return $this->action;
	}
	
	/**
	 * Ошибка работы движка,шаблон грузится из статической страницы
	 * @param string $message - сообщение об ошибки
	 */
	static function GeneratePageFatalError($message){
		$FatalError = $message;
		require_once 'application/core/Error.php';
		die();
	}
}