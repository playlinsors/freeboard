<?php 

class user_controller{
	function __construct($action){
		
		if ($_SESSION['USERDATA_id']==null) 
			FrontController::GenerateHeaderLocation('200 ok','');
		
		$SiteBody = TemplateMenuBuilder::getInstans();
		$Content = $this->defaultAction();
		
		if ($action=='exitaction') $Content = $this->exitAction();
		
		if ($action=='editaction') $Content = $this->editAction();
		
		if ($action=='editsaveaction') $Content = $this->editsaveAction();
		
		$SiteBody->setTemplateTextMain($Content);
		
		}
	/**
	 * Действие по умолчанию,отображается сам сайт
	 */
	private function defaultAction(){
		$FC = FrontController::getInstans();
		$NameModel = $FC->GetCurrentPage().'_model';
		$IncludePathModel = 'application/models/'.$NameModel.'.php';

		if (file_exists($IncludePathModel)){
			require_once $IncludePathModel;
			if (class_exists($NameModel)){
	
				$View = new $NameModel();
				return $View->RenderView($FC->GetCurrentPage());
				
			} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindClassModels.$NameModel.':'.$IncludePathModel);
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindModels.$IncludePathModel);
	}
	
	private function exitAction(){
		$FC = FrontController::getInstans();
		$NameModel = $FC->GetCurrentPage().'_exit_model';
		$IncludePathModel = 'application/models/'.$NameModel.'.php';

		if (file_exists($IncludePathModel)){
			require_once $IncludePathModel;
			if (class_exists($NameModel)){
	
				$View = new $NameModel();
				return $View->RenderView($FC->GetCurrentPage());
				
			} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindClassModels.$NameModel.':'.$IncludePathModel);
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindModels.$IncludePathModel);
	}
	
	private function editAction(){
		$FC = FrontController::getInstans();
		$NameModel = $FC->GetCurrentPage().'_edit_model';
		$IncludePathModel = 'application/models/'.$NameModel.'.php';

		if (file_exists($IncludePathModel)){
			require_once $IncludePathModel;
			if (class_exists($NameModel)){
	
				$View = new $NameModel();
				return $View->RenderView($FC->GetCurrentPage());
				
			} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindClassModels.$NameModel.':'.$IncludePathModel);
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindModels.$IncludePathModel);
	}
	
	private function editsaveAction(){
		$FC = FrontController::getInstans();
		$NameModel = $FC->GetCurrentPage().'_editsave_model';
		$IncludePathModel = 'application/models/'.$NameModel.'.php';

		if (file_exists($IncludePathModel)){
			require_once $IncludePathModel;
			if (class_exists($NameModel)){
	
				$View = new $NameModel();
				return $View->RenderView($FC->GetCurrentPage());
				
			} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindClassModels.$NameModel.':'.$IncludePathModel);
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindModels.$IncludePathModel);
	}
}