<?php 

/**
 * @author Admin
 */
class mail_controller{
	function __construct($action){
		
		$SiteBody = TemplateMenuBuilder::getInstans();
		if ($action=='sendmailaction') {
			$SiteBody->setTemplateTextMain($this->sendAction());
			$SiteBody->setTemplateLeftMenu(null);
		} else {
			$SiteBody->setTemplateTextMain($this->defaultAction());
			$SiteBody->setTemplateLeftMenu(null);
			}
		}
	/**
	 * Действие по умолчанию,отображается сам сайт
	 */
	private function defaultAction(){
		$FC = FrontController::getInstans();
		$NameModel = $FC->GetCurrentPage().'_model';
		$IncludePathModel = 'application/models/'.$NameModel.'.php';

		if (file_exists($IncludePathModel)){
			require_once $IncludePathModel;
			if (class_exists($NameModel)){
	
				$View = new $NameModel();
				return $View->RenderView($FC->GetCurrentPage());
				
			} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindClassModels.$NameModel.':'.$IncludePathModel);
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindModels.$IncludePathModel);
	}
	private function sendAction(){
		$FC = FrontController::getInstans();
		$NameModel = $FC->GetCurrentPage().'_sendmailaction_model';
		$IncludePathModel = 'application/models/'.$NameModel.'.php';

		if (file_exists($IncludePathModel)){
			require_once $IncludePathModel;
			if (class_exists($NameModel)){
	
				$View = new $NameModel();
				return $View->RenderView($FC->GetCurrentPage());
				
			} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindClassModels.$NameModel.':'.$IncludePathModel);
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindModels.$IncludePathModel);
		
		
	}
	
	
}