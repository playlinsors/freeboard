<?php 
/**
 * Админка 
 */
class administrator_controller{
	function __construct($action){
		
		if (($_SESSION['USERDATA_id']==null) || ($_SESSION['USERDATA_ACCESS']['AccessOnControlPanel']==0)) 
			FrontController::GeneratePageFatalError("Невозможно получить доступ");
		
		$SiteBody = TemplateMenuBuilder::getInstans();
		
		switch ($action) {
			case 'regionmanageraction':
				$SiteBody->setTemplateTextMain($this->regionmanagerAction());
				$SiteBody->setTemplateLeftAdminMenu('Left_administrator_region_menu');
				$SiteBody->setTemplateLeftMenu('');
				break;
			default:
				$SiteBody->setTemplateTextMain($this->regionmanagerAction());
		}	
	}
	/**
	 * Действие по умолчанию,отображается сам сайт
	 */
	private function regionmanagerAction(){
		$FC = FrontController::getInstans();
		$NameModel = $FC->GetCurrentPage().'_regionmanager_model';
		$IncludePathModel = 'application/models/'.$NameModel.'.php';

		if (file_exists($IncludePathModel)){
			require_once $IncludePathModel;
			if (class_exists($NameModel)){
	
				$View = new $NameModel();
				return $View->RenderView($FC->GetCurrentPage());
				
			} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindClassModels.$NameModel.':'.$IncludePathModel);
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindModels.$IncludePathModel);
	}
	
	
}