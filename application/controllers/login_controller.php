<?php 
/**
 * Контролеер входа в систему
 * @author PlayLinsor
 */
class login_controller{
	function __construct($action){
		$Content = '';
		
		if ($_SESSION['USERDATA_id']!=null) 
			FrontController::GenerateHeaderLocation('200 ok','?page=user');
		
		$SiteBody = TemplateMenuBuilder::getInstans();
		if ($action=='enteraction') $Content = $this->enterAction();
			else $Content = $this->defaultAction();
		
		$SiteBody->setTemplateTextMain($Content);
		}
	/**
	 * Действие по умолчанию,отображается сам сайт
	 */
	private function defaultAction(){
		$FC = FrontController::getInstans();
		$NameModel = $FC->GetCurrentPage().'_model';
		$IncludePathModel = 'application/models/'.$NameModel.'.php';

		if (file_exists($IncludePathModel)){
			require_once $IncludePathModel;
			if (class_exists($NameModel)){
	
				$View = new $NameModel();
				return $View->RenderView($FC->GetCurrentPage());
				
			} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindClassModels.$NameModel.':'.$IncludePathModel);
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindModels.$IncludePathModel);
	}
	
	private function enterAction(){
		$FC = FrontController::getInstans();
		$NameModel = $FC->GetCurrentPage().'_enter_model';
		$IncludePathModel = 'application/models/'.$NameModel.'.php';

		if (file_exists($IncludePathModel)){
			require_once $IncludePathModel;
			if (class_exists($NameModel)){
	
				$View = new $NameModel();
				return $View->RenderView($FC->GetCurrentPage());
				
			} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindClassModels.$NameModel.':'.$IncludePathModel);
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindModels.$IncludePathModel);
	}
		
}