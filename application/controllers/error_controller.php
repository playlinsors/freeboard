<?
/**
 * Контроллер для вывода об недоступности страницы
 * @author Admin
 */
class error_controller{
	function __construct($action){
		$Site = TemplateMenuBuilder::getInstans();
		$Site->setTemplateTextMain($this->defaultAction());
		$Site->setTemplateLeftMenu(null);
		}
		
	
	public function defaultAction(){
		$IncludePathModel = 'application/models/error_model.php';
		if (file_exists($IncludePathModel)){
			require_once $IncludePathModel;
			$ErrorView = new error_model();
			return $ErrorView->RenderView('error');
				
			} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindModels.$IncludePathModel);
		}		
		
	}