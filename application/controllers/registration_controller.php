<?php 
/**
 * Контролеер индексного файла
 * @author PlayLinsor
 * @todo Переделать, сильно грязно.
 * @todo experients data что то сделать
 */
class registration_controller{
	function __construct($action){
		
		if ($_SESSION['USERDATA_id']!=null) 
			FrontController::GenerateHeaderLocation('200 ok','?page=user');
		
		$SiteBody = TemplateMenuBuilder::getInstans();
		if ($action=='confirmemailaction'){
			$SiteBody->setTemplateTextMain($this->confirmemailAction());
		} elseif ($action=='sendaction') {
			$SiteBody->setTemplateTextMain($this->sendAction());
		} else 
			$SiteBody->setTemplateTextMain($this->defaultAction());
		}
	/**
	 * Действие по умолчанию,отображается сам сайт
	 */
	private function defaultAction(){
		$FC = FrontController::getInstans();
		$NameModel = $FC->GetCurrentPage().'_model';
		$IncludePathModel = 'application/models/'.$NameModel.'.php';

		if (file_exists($IncludePathModel)){
			require_once $IncludePathModel;
			if (class_exists($NameModel)){
	
				$View = new $NameModel();
				return $View->RenderView($FC->GetCurrentPage());
				
			} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindClassModels.$NameModel.':'.$IncludePathModel);
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindModels.$IncludePathModel);
	}
	
	/**
	 * Если нажата кнопка подтверждения регистрации
	 */
	private function sendAction(){
		$FC = FrontController::getInstans();
		$NameModel = $FC->GetCurrentPage().'_send_model';
		$IncludePathModel = 'application/models/'.$NameModel.'.php';

		if (file_exists($IncludePathModel)){
			require_once $IncludePathModel;
			if (class_exists($NameModel)){
	
				$View = new $NameModel();
				return $View->RenderView($FC->GetCurrentPage());
				
			} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindClassModels.$NameModel.':'.$IncludePathModel);
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindModels.$IncludePathModel);
	}
	/**
	 * Подтверждения с емайла
	 */
	private function confirmemailAction(){
		$FC = FrontController::getInstans();
		$NameModel = $FC->GetCurrentPage().'_confirmemail_model';
		$IncludePathModel = 'application/models/'.$NameModel.'.php';

		if (file_exists($IncludePathModel)){
			require_once $IncludePathModel;
			if (class_exists($NameModel)){
	
				$View = new $NameModel();
				return $View->RenderView($FC->GetCurrentPage());
				
			} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindClassModels.$NameModel.':'.$IncludePathModel);
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindModels.$IncludePathModel);
	}
	
	
}