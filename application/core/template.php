﻿<?php
$TemplateModule = TemplateMenuBuilder::getInstans(); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?=LangTextSring::$TextTitleSite?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
<link rel="stylesheet" type="text/css" href="styles/default.css" title="default"/>
<link rel="stylesheet" type="text/css" href="styles/font-awesome.min.css" title="default"/>
</head>
<body>
<div>
  <div id="Inner">
    <div id="Container">
      <div id="Head">
        <div id="Head_left">
          <div id="Leaf_top"><a href='http://<?=$_SERVER['HTTP_HOST']?>'></a></div>
          <div id="Leaf_bottom"> 
			<?=$TemplateModule->getTemplateUserRegistrationMenu()?>
		  </div>
        </div>
        <div id="Head_right">
          <div id="Logo">
            <div id="Name"><?=LangTextSring::$TextHeadersSite?></div>
            <div id="Informations"><?=LangTextSring::$TextSubHeadersSite?></div>
          </div>
          <div id="Top_menu"> 
		  <?=$TemplateModule->getTemplateHeaderMenu() ?>
		</div>
        </div>
      </div>
      <div id="CentralPart">
        <div id="LeftPart">
          <?=$TemplateModule->getTemplateLeftTopMenu()?> 
          
		  <?=$TemplateModule->getTemplateLeftAdminMenu()?> 
		  	
          <?=$TemplateModule->getTemplateLeftMenu()?> 
        </div>
        <?php 
        if (($TemplateModule->getTemplateLeftMenu()!=null) || ($TemplateModule->getTemplateLeftTopMenu()!=null) || ($TemplateModule->getTemplateLeftAdminMenu()!=null))
        	echo '<div id="RightPart" >
        			<div id="Page">
        				<div id="Page_header" >';
        else echo '<div id="RightPart" style="width:100%">
        			<div id="Page" style="width:99%">
        				<div id="Page_header"  style="width:100%">';
        
        echo $TemplateModule->getTemplateTextMain();
        ?>
        </div></div></div>
      <div id="Bottom" style="margin-top:20px">
        <p style="display: none;"><a href="http://www.sunlight.cz">Design by: Sunlight webdesign</a></p>
		<p class="down" > Copyright &copy; 2014, Develop by Naymko Dmitriy<strong><br />PlayLinsor@gmail.com</strong></p>
        <div class="down2">
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</body>
</html>