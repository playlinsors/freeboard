<?php
class DataBaseFunction{
	private static $ClassHandler = null;
	private $DB_CONNECT;
	
	private function __construct(){
		$_SESSION['DEBUG_CONNECT_BASE'] +=1;
		$this->DB_CONNECT = mysql_connect($GLOBALS['DB_HOST'],$GLOBALS['DB_USER'],$GLOBALS['DB_PASS']);
		mysql_select_db($GLOBALS['DB_BASE'],$this->DB_CONNECT);
		self::Query('SET CHARSET utf8');
	}
	
	public static function getInstans(){
		if (self::$ClassHandler == null) 
			self::$ClassHandler = new self();
		return self::$ClassHandler;
	}
	
	public function CheckAndEchoError(){
		$error = mysql_error();
		if ($error!=null)
			FrontController::GeneratePageFatalError($error."Connect = ".$_SESSION['DEBUG_CONNECT_BASE']." Query=".$_SESSION['DEBUG_QUERY_BASE']);
	}
	
	public function Query($query){
		$_SESSION['DEBUG_QUERY_BASE'] +=1;
		$result = mysql_query($query,$this->DB_CONNECT);
		self::CheckAndEchoError();
		return $result;
	}
	
	public function __destruct(){
		mysql_close($this->DB_CONNECT);
		$_SESSION['DEBUG_QUERY_BASE'] = 0;
		$_SESSION['DEBUG_CONNECT_BASE'] -= 1;
	}

}