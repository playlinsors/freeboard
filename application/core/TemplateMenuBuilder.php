<?php
/**
 * Класс для связи всех менюшек и их построение в единое целое с учетом шаблона сайта
 * @author PlayLinsor
 * @version 1.0.2n
 * @copyright 13.01.14
 */
class TemplateMenuBuilder{
	private $Tmpl_UserRegistration,$Tmpl_HeaderMenu,$Tmpl_LeftTopMenu,$Tmpl_LeftAdminMenu,$Tmpl_LeftMenu,$Tmpl_TextMain;
	private static $ClassHandler;
	
	private function __construct(){
		$this->InitialBildSite();
	}
	
	static public function getInstans(){
		if (self::$ClassHandler==null)
			self::$ClassHandler = new self();
		return self::$ClassHandler;
	}
	
	/**
	 * Загрузка менюшек по умолчанию
	 */
	private function InitialBildSite(){
		require_once 'application/models/LeftMenu_model.php';
		require_once 'application/models/BarStatusUserRegistration_model.php';
		require_once 'application/models/HeaderMenu_model.php';
		
		$LeftMenu = new LeftMenu_model();
		$this->Tmpl_LeftMenu = $LeftMenu->RenderView();
				
		$UserRegStatusBar = new BarStatusUserRegistration_model();
		$this->Tmpl_UserRegistration = $UserRegStatusBar->RenderView();
				
		$HeaderMenu = new HeaderMenu_model();
		$this->Tmpl_HeaderMenu = $HeaderMenu->RenderView();
		
		$this->Tmpl_TextMain = 'hello world ';
	}
	
	/**
	 * грузит шаблон отображения и вставляет необходимые модули(менюшки)
	 */
	public function ShowBildedSite(){
		require_once 'application/core/template.php';
	}

	// get/set template site varible
	
	public function getTemplateLeftMenu(){
		return $this->Tmpl_LeftMenu;
	}
	
	public function getTemplateLeftTopMenu(){
		return $this->Tmpl_LeftTopMenu;
	}
	
	public function getTemplateLeftAdminMenu(){
		return $this->Tmpl_LeftAdminMenu;
	}
	
	public function getTemplateHeaderMenu(){
		return $this->Tmpl_HeaderMenu;
	}
	
	public function getTemplateUserRegistrationMenu(){
		return $this->Tmpl_UserRegistration;
	}
	
	public function getTemplateTextMain(){
		return $this->Tmpl_TextMain;
	}
					////////////////////////////////////////////////////////////
	/**
	 * Установка левого меню
	*/
	public function setTemplateLeftMenu($template){
		$this->Tmpl_LeftMenu = $template;
	}
	/**
	 * Установка левого верхнего меню
	 * @param $model Указывать модель загружаемого файла
	*/
	public function setTemplateLeftTopMenu($model){
		require_once 'application/models/'.$model.'_model.php';
		$LeftTopMenu = new LeftTopMenu_model();
		$this->Tmpl_LeftTopMenu = $LeftTopMenu->RenderView();
	}
	
	/**
	 * Установка левого Административного меню
	 * @param $model Указывать модель загружаемого файла
	 */
	public function setTemplateLeftAdminMenu($model){
		$model .="_model";
		require_once 'application/models/'.$model.'.php';
		$LeftAdminMenu = new $model;
		$this->Tmpl_LeftAdminMenu = $LeftAdminMenu->RenderView();
	}
	
	/**
	 * Установка верхнего меню
	 */
	public function setTemplateHeaderMenu($template){
		$this->Tmpl_HeaderMenu = $template;
	}
	/**
	 * Установка о статусе пользователя
	 */
	public function setTemplateUserRegistrationMenu($template){
		$this->Tmpl_UserRegistration = $template;
	}
	/**
	 * Установка основного контента
	 */
	public function setTemplateTextMain($template){
		$this->Tmpl_TextMain = $template;
	}
	
	
	
	
	
	
	
}