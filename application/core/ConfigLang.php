<?php
/**
 * Что то типо языкового пакета
 * @author Admin
 * 
 */
class LangTextSring{
	// General Labels on site
	static $TextSiteName = 'freeboard.hol.es';
	static $TextHeadersSite = 'Доска Обьявлений';
	static $TextSubHeadersSite = 'Бесплатная доска обьявлений по городам';
	static $TextTitleSite = 'Доска Обьявлений';
	//
	static $TextNotesTypeSell = 'Продам';
	static $TextNotesTypeBuy = 'Куплю';
	static $TextNotesListSell = ' Продаю ';
	static $TextNotesListBuy = ' Покупаю ';
	//
	
	// Menu items
	static $TextMenuTemplateRegistration = 'Регистрация';
	static $TextMenuTemplateLogin = 'Вход';
	static $TextMenuTemplateExit = 'Выход';
	static $TextMenuTemplateProfile= 'Профиль';
	static $TextMenuTemplateLabelRegion = 'Регионы';
	static $TextMenuTemplateLabelRegion2 = 'Все регионы';
	// Errors and System Warnings
	static $ErrorFindModels = 'Не могу найти файл модели ';
	static $ErrorFindClassModels = 'Не могу найти необходимый класс модели ';
	static $ErrorFindClassController = 'Не могу найти необходимый класс контроллера ';
	static $ErrorFindView = 'Не могу найти файл Вьювера ';
	
}