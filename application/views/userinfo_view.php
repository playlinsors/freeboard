<?php 
$ArrayData = self::GetArrayUserData();

if ($ArrayData==NULL) {
	echo '<div id="main_text">Пользователь не найден</div>';
} else {
?>
			<h1>Профиль пользователя <?=$ArrayData['login'] ?></h1>             
  		<div id="main_text" align="center">
<table class="mainfoncolor registration" style="margin-left:0;align:center">
  <tr>
    <td colspan="2" align="center"  style="background-color:#E1F1F1">
    <strong>Общие сведения</strong>
    </td>
  </tr>
  <tr class="row1">
    <td>Ф.И.О</td>
    <td><?=$ArrayData['first name']." ".$ArrayData['name']." ".$ArrayData['last name'] ?></td>
  </tr>
  <tr class="row1">
    <td width="270px">Роль</td>
    <td width="380px"><strong><?=$ArrayData['nameRole'] ?></strong></td>
  </tr>
  <?php
  if ($ArrayData['birthday']) {
   echo '<tr class="row1"><td width="270px">Дата рождения</td>
    <td width="380px">'.$ArrayData['birthday'].'</td></tr>';
  }
   ?>
   <tr class="row1">
    <td width="270px">Пол</td>
    <td width="380px"><?=self::GetUserGenderLabel($ArrayData['gender']) ?></td>
  </tr>
  
  <tr>
    <td colspan="2" align="center"  style="background-color:#E1F1F1"><strong> Данные для связи</strong> </td>
  </tr>
  
  <?php 
  if ($ArrayData['Organization']!=null) {
  	echo '<tr class="row1"><td>Организация</td><td>';
  	echo $ArrayData['Organization'];
  	echo '</td></tr>';
  }
 if ($ArrayData['Sity']!=null) {
  	echo '<tr class="row1"><td>Город</td><td>';
  	echo $ArrayData['Sity'];
  	echo '</td></tr>';
  }
 if ($ArrayData['Telephone']!=null) {
  	echo '<tr class="row1"><td>Телефон</td><td>';
  	echo $ArrayData['Telephone'];
  	echo '</td></tr>';
  }
  ?>
  <tr class="row1">
    <td>Электронная почта</td>
    <td><?=$ArrayData['email']?></td>
  </tr>
  <tr>
    <td colspan="2" align="center"  style="background-color:#E1F1F1"><strong>Информация обо мне</strong> </td>
  </tr>
  <tr class="row1">
    <td colspan="2" style="padding:10px 20px;text-align:justify">
    <?=$ArrayData['info'] ?>
    </td>
  </tr>
  
  <tr class="row1">
    <td colspan="2" align="center">
    <?php 
    $TextData = "";
		if ($ArrayData['login']==$_SESSION['USERDATA_login']) {
			$TextData = '<form name="registration" method="post" action="?page=user&action=edit">
						<input type="submit" name="Submit" value=" Изменить данные о себе " />
						</form>';
		} else {
			$TextData = '<form name="registration" method="post" action="?page=mail">
						<input type="hidden" name="user" value="'.$ArrayData['login'].'">
						<input type="submit" name="Submit" value=" Написать письмо " />
    					</form>';
		}
	
		echo $TextData;
    ?>
   	    
    	
    </td>
  </tr>
  
  </table>
  		<br>
		
  		</div>
      	 
<?php }
?>