<?php 
$Id = self::Notes_getFilteredID();
$ArrayData = self::NotesArray_NoteFullInfo($Id);
$NoteType = self::Notes_getTypeNote($ArrayData['typenotes']);
if ($ArrayData==null) {
	$NoteDataHead = '<div align="center"><strong>Обьявление не найдено в базе, возможно оно было удалено</strong></div>';
} else {
$NoteDataHead=<<<DataHead
<table class="mainfoncolor registration" style="margin-left:0">
  <tr>
    <td colspan="2" align="center"  style="background-color:#E1F1F1"><strong>
    {$ArrayData['Tinyinfo']}
	</strong></td>
  </tr>
  <tr class="row1">
    <td width="270px">Категория</td>
    <td width="380px"><strong>{$ArrayData['nameRegion']}</strong></td>
  </tr>
  <tr class="row1">
    <td>Раздел</td>
    <td>{$ArrayData['NameSubregion']}</td>
  </tr>
  <tr class="row2">
    <td>Дата публикации</td>
    <td>{$ArrayData['date']}</td>
  </tr>
  <tr class="row1">
    <td>Тип операции</td>
    <td><strong>{$NoteType}</strong></td>
  </tr>
  <tr>
    <td colspan="2" align="center"  style="background-color:#E1F1F1">Текст обьявления</td>
  </tr>
  <tr class="row1">
    <td colspan="2" style="padding:10px 20px;text-align:justify">
    {$ArrayData['infodata']}
    </td>
  </tr>
DataHead;

	if ($ArrayData['photoPath1']!=null){
		$DataImage = '<tr><td colspan="2" align="center"  style="background-color:#E1F1F1">
		<strong>Прикрепленные картинки </strong>(нажмите на картинку для просмотра оригинала)</td>
  		</tr><tr><td colspan="2" align="center"  class="row1">';
		$TempDate = "";
		for ($i = 1; $i <= 4; $i++) {
			$ImagePath = $ArrayData['photoPath'.$i];
			$NotesSettings_pathtoImage = $GLOBALS['NotesSettings_pathtoImage'];
			if ($ImagePath!=null){
				$TempDate .= '<a href="'.$NotesSettings_pathtoImage.$ImagePath.'">
   						  	  <img src="'.$NotesSettings_pathtoImage.$ImagePath.'" width="64" height="64">';
			}
		}
		$DataImage = $DataImage.$TempDate.'</td></tr>';
	}

$NoteField_main='<tr><td colspan="2" align="center"  style="background-color:#E1F1F1">
<strong> Данные для связи </strong></td></tr>';	

if ($ArrayData['login']!=NULL) {
	$NoteField_UserLink = '<tr class="row1"><td>Пользователь</td>
    <td><a class="alink" href="?page=user&action=userid&id='.$ArrayData['Uid'].'"><strong>'.$ArrayData['login'].'</strong></a></td></tr>';
}
if ($ArrayData['Name']!=NULL) {
	$NoteField_Name = '<tr class="row1"><td>Имя</td>
    <td>'.$ArrayData['Name'].'</td></tr>';
}
if ($ArrayData['telephone']!=NULL) {
	$NoteField_Tel = '<tr class="row1"><td>Телефон</td>
    <td>'.$ArrayData['telephone'].'</td></tr>';
}
if ($ArrayData['sity']!=NULL) {
	$NoteField_Sity = '<tr class="row1"><td>Город</td>
    <td>'.$ArrayData['sity'].'</td></tr>';
}
$NoteDataBottom =<<<Bottom
<tr class="row1">
    <td colspan="2" align="center">
   	    <form name="registration" method="post" action="?page=mail">
   	    <input type="hidden" name="ActionAs" value="onNotes" />
   	    <input type="hidden" name="noteId" value="$Id " />
   	    <input type="hidden" name="user" value="{$ArrayData['login']}" />
    	<input type="submit" name="Submit" value="Написать письмо автору обьявления" />
    	</form>
    </td>
  </tr>
  </table>	
Bottom;


}
?>

<div id="main_text_notes" align="center">
	<?=$NoteDataHead ?>	
	<?=$DataImage ?>
	<?=$NoteField_main ?>
	<?=$NoteField_UserLink ?>
	<?=$NoteField_Name ?>
	<?=$NoteField_Tel ?>
	<?=$NoteField_Sity ?>
	<?=$NoteDataBottom ?>		
		
</div>	 
