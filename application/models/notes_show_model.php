<?php
class notes_show_model{
	private $DataBaseHandle = null;
	
	function RenderView($ViewFile){
		$IncludePathViewFile = 'application/views/'.$ViewFile.'_show_view.php';
		
		if (file_exists($IncludePathViewFile)){
			ob_start();
				
			$this->DataBaseHandle = DataBaseFunction::getInstans();
			
			require_once $IncludePathViewFile;
			
			return ob_get_clean();
			
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindView.$IncludePathViewFile);
	}
	
	public function NotesArray_NoteFullInfo($id){
		if ($id==0) return null;
		$resultArray = array();
		$query = "SELECT notes.`typenotes`,notes.`Tinyinfo`,notes.`infodata`,notes.`Name`,
					notes.`email`,notes.`telephone`,notes.`sity`,notes.`photoPath1`,notes.`photoPath2`,
					notes.`photoPath3`,notes.`photoPath4`,notes.`date`,regions.`nameRegion`,
					subregions.`nameRegion` AS 'NameSubregion',users.login,users.id AS Uid
				FROM `notes`
			    LEFT JOIN users ON notes.idLogin=users.id
			    LEFT JOIN regions ON notes.idRegion=regions.id
				LEFT JOIN subregions ON notes.idSubregion=subregions.id
				WHERE notes.id='$id';";
		$result = $this->DataBaseHandle->Query($query);
		while ($row = mysql_fetch_assoc($result)){
		$resultArray[] = $row;
		}
		return $resultArray[0];
	}
	
	public function Notes_getFilteredID(){
		$ID = $_GET['id'];
		if (($ID<0) || ($ID==null)) return 0;
		$ID = $ID*1;
		return $ID;
	}
	
	public function Notes_getTypeNote($id){
		$Text = "неизвестно";
		if ($id==1) $Text = LangTextSring::$TextNotesTypeSell;
		if ($id==2) $Text = LangTextSring::$TextNotesTypeBuy;
		return $Text;
	}
}