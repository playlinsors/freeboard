<?php
class userinfo_model{
	function RenderView($ViewFile){
		$IncludePathViewFile = 'application/views/'.$ViewFile.'_view.php';
		
		if (file_exists($IncludePathViewFile)){
			ob_start();
			
			if (($GLOBALS['UserInfo_showInfoGuest']==false) && ($_SESSION['USERDATA_id']==0)) {
				$outputText = "Гостям доступ запрещен";
				$IncludePathViewFile = 'application/views/INFO_MESSAGE_view.php';
				
			}
			
			require_once $IncludePathViewFile;
			
			return ob_get_clean();
			
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindView.$IncludePathViewFile);
	}
	
	function GetUserRole($IdRole){

		$query = "SELECT nameRole FROM `users_roles` WHERE id='".$IdRole."'";
		
		$DataBaseHandle = DataBaseFunction::getInstans();
		
		$result = $DataBaseHandle->Query($query);
		$row = mysql_fetch_assoc($result);
		
		return $row['nameRole'];
	}
	function GetArrayUserData(){
		
		$ID = $_GET['id'];
		if (($ID<0) || ($ID==null)) return null;
		$ID = $ID*1;
		
		$DataBaseHandle = DataBaseFunction::getInstans();
		$query = "SELECT `login`,`name`,`first name`,`last name`,`gender`,`email`,`DataReg`,`Sity`,`Organization`,`Telephone`,`info`,`birthday`,`confirmemail`,users_roles.nameRole
					FROM users
					LEFT JOIN users_roles ON users.access = users_roles.id
					WHERE users.id = '".$ID."';";
		$result = $DataBaseHandle->Query($query);
		$row = mysql_fetch_assoc($result);
		
		return $row;
	}
	
	
	function GetUserGenderLabel($Id){
		if ($Id==1) return 'Мужской';
		if ($Id==2) return 'Женский';
		return 'Неизвестно';
	}
}