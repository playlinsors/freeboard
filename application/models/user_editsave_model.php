<?php
class user_editsave_model{
	function RenderView($ViewFile){
		$IncludePathViewFile = 'application/views/'.$ViewFile.'_edit_view.php';
		
		if (file_exists($IncludePathViewFile)){
			ob_start();
			$registration_errorText = null;
			$idUser = $_SESSION['USERDATA_id'];
			
			if ($_POST['captha']==$_SESSION['captcha_keystring']){
				if ($_POST['login']!=$_SESSION['USERDATA_login']){
					$errorText="Нет доступа к редактированию";
				}
				
				if (($_POST['email']==null) || ($_POST['name']==null)){
					$errorText="Не заполнены основные поля";
				}
				
				if ($_POST['password']!=null){
					if ($this->CheckLastPassword($idUser, $_POST['password'])==true){
						if ($_POST['newPassword']==$_POST['newPassword2']){
							if (strlen($_POST['newPassword'])>5){
								$passwordChange = $this->ClearData($_POST['newPassword']);
							} else $errorText = "Сильно короткий пароль";
						} else $errorText = "Не совпадает новые пароли";
					} else $errorText = "Не верный старый пароль";
				}
				
				if ($errorText==null){
					
					if ($this->SaveUser($idUser,$passwordChange)==1)	$outputText = "Сохранение данных прошло успешно";
						else $outputText="Невозможно изменить некоторые данные";
						
					$IncludePathViewFile = 'application/views/INFO_MESSAGE_view.php'; 
				} 

			} else $errorText ="Ошибка, неправильно введен проверочный код";
			
			require_once $IncludePathViewFile;
			
			return ob_get_clean();
			
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindView.$IncludePathViewFile);
	}
	
	/**
	 * Проверка основных данных и Сохранение в ссесию
	 */
	function ClearData($text){
		$text = trim($text);
		$text = htmlspecialchars($text);
		$text = mysql_escape_string($text);
		return $text;
	}
	
	function CheckLastPassword($UserId,$pass){
			$DB = DataBaseFunction::getInstans();
			$result = $DB->Query('SELECT password FROM `users` WHERE `id`="'.$UserId.'"');
			$row = mysql_fetch_array($result);
			if ($row[0]!=null) {
				if ($row[0]==md5($this->ClearData($pass))) return true;
			}
		return false;
	}
	
	
	function SendEmail($ComfirmHash){	
		$to      = $_POST['email'];
		$subject = 'Смена почтового ящика на '.LangTextSring::$TextSiteName ;
		$message = 'Приветствуем Вас, '.$_POST['login']." <br><br>".
              "Для подтверждения смены почтового ящика, пожалуйста, нажмите на ссылку ниже: <br><br>".
              "http://".$_SERVER['HTTP_HOST']."/?page=registration&action=confirmemail&value=".$ComfirmHash." <br><br>".
              "С уважением, администрация сайта ".LangTextSring::$TextSiteName."<br><br>";
        
		$headers = 	"MIME-Version: 1.0\r\n";
		$headers .= 'Content-Type: text/html;charset="utf-8"'."\r\n";  
		$headers .= 'From: '.$GLOBALS['ADMINISTRATOR_EMAIL']."\r\n";
		$headers .= "X-Mailer: My Send E-mail \r\n";
			
		mail($to, $subject, $message, $headers);
		
	
	}
	
	function SaveUser($IdUser,$changePassword=null){
		$genderFiltered = $this->ClearData($_POST['gender']);
		$genderFiltered = ($genderFiltered==1) ? '1':'2';
		
		if ($changePassword!=null) $AddedChancePassword = ",`password` = '".md5($changePassword)."'";
			else $AddedChancePassword = null;
			
		$query = "UPDATE `users` SET 
						`name` = '".$this->ClearData($_POST['name'])."',
						`first name` = '".$this->ClearData($_POST['firstname'])."',
						`last name` = '".$this->ClearData($_POST['lastname'])."',
						`gender` = '".$genderFiltered."',
						`email` = '".$this->ClearData($_POST['email'])."',
						`Sity` = '".$this->ClearData($_POST['sity'])."',
						`Organization` = '".$this->ClearData($_POST['organization'])."',
						`Telephone` = '".$this->ClearData($_POST['Telephone'])."',
						`info` = '".$this->ClearData($_POST['about'])."',
						`birthday` = '".$this->ClearData($_POST['birth'])."' 
						$AddedChancePassword
						WHERE `id` =".$IdUser."; ";
		$DB = DataBaseFunction::getInstans();
		$result = $DB->Query($query);
		
		$query = 'SELECT * FROM `users` WHERE `id`="'.$IdUser.'" ; ';
		$result = $DB->Query($query);
		
		$row = mysql_fetch_assoc($result);
		foreach ($row as $key => $value) {
			$_SESSION['USERDATA_'.$key] = $value;
		}
		
		if ($result==false) FrontController::GeneratePageFatalError("Что то произошло с базой данных");
			else return 1;
	
	}
}