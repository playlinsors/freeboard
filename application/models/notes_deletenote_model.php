﻿<?php
class notes_deletenote_model{
	function RenderView($ViewFile){
		$IncludePathViewFile = 'application/views/INFO_MESSAGE_view.php';
		
		if (file_exists($IncludePathViewFile)){
			ob_start();
			
			if ($_GET['hash']==null) {
				$outputText = self::notes_one_enter();
			} else {
				$outputText = self::notes_two_enter();
			}
			
			require_once $IncludePathViewFile;
			
			return ob_get_clean();
			
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindView.$IncludePathViewFile);
	}
	
	function notes_one_enter(){
		$ComfirmHash = md5(rand(10000,99999));
		$_SESSION['ComfirmHashOnDelete'] = $ComfirmHash;
		$outputText = '<div align="center" >Внимание, вы действительно хотите удалить объявление, это действие необратимо !<br><br><strong>
						   <a class="alink" href="'.$_SERVER['REQUEST_URI'].'&hash='.$ComfirmHash.'">Подтвердить удаление</a></strong></div>';
		return $outputText;
	}
	
	function notes_two_enter(){
		if ($_SESSION['USERDATA_id']==0) {
			$ReturnText = "Доступ к  этой функции невозможен";
		} 
		$ID = $_GET['id'];
		if (($ID<0) || ($ID==null)) {
			$ReturnText = "Не найден индификатор в БД";
		}
			
		if ($_GET['hash']==$_SESSION['ComfirmHashOnDelete'] && (!$ReturnText)){
				$ID = $ID*1;
				$query = "SELECT notes.idLogin
							FROM  notes
							WHERE notes.id = '".$ID."'";
				
				$_SESSION[''] = md5(rand(10000,99999));
				
				$DB = DataBaseFunction::getInstans();
				$result = $DB->Query($query);
				$row = mysql_fetch_array($result);
				
				if ($row['idLogin']==$_SESSION['USERDATA_id'] || ($_SESSION['USERDATA_ACCESS'][0]==1)) {
					return self::notes_deletenote($ID);
				} else $ReturnText = "Доступ открыт только для Администратора сайта";
			
		} else $ReturnText = "Не совпадает индификатор подтверждения, попробуйте еще раз";
		return  $ReturnText;
	}
	
	function notes_deletenote($FiltredId){
		$DB = DataBaseFunction::getInstans();
		$query = "SELECT notes.IdRegion,notes.idSubregion,notes.miniPhotoPath,notes.photoPath1,
					notes.photoPath2,notes.photoPath3,notes.photoPath4
					From notes
					WHERE notes.id='".$FiltredId."';";
					
		$result = $DB->Query($query);
		$row = mysql_fetch_array($result);
		$NotesSettings_pathtoImage = $GLOBALS['NotesSettings_pathtoImage'];
		
		if ($row['miniPhotoPath']!=null) {
			if (file_exists($NotesSettings_pathtoImage.$row['miniPhotoPath'])) unlink($NotesSettings_pathtoImage.$row['miniPhotoPath']); 
		}
		for ($i=1;$i<=4;$i++){
			if ($row['photoPath'.$i]!=null) { 
				if (file_exists($NotesSettings_pathtoImage.$row['photoPath'.$i])) unlink($NotesSettings_pathtoImage.$row['photoPath'.$i]);
			}
		}
		
	 	$query = "UPDATE `regions` SET `count_notes`= `count_notes` -1 WHERE  regions.id = '".$row['IdRegion']."';";
	 	$DB->Query($query);
	 	
	 	$query = "UPDATE `subregions` SET `count_notes`= `count_notes` -1 WHERE  subregions.id = '".$row['idSubregion']."';";
	 	$DB->Query($query);
	 	
	 	$query = "DELETE FROM `notes` WHERE  `id`='".$FiltredId."';";
	 	$DB->Query($query);
	 	
	 	return "Обьявление успешно удалено";
	 	
	}
}