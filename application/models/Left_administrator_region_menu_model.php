<?php
class Left_administrator_region_menu_model{	
	function RenderView(){
		$IncludePathViewFile = 'application/views/Left_administrator_region_menu_view.php';
		if (file_exists($IncludePathViewFile)){
			ob_start();	
			
			require_once $IncludePathViewFile;
			
			return ob_get_clean();
			
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindView.$IncludePathViewFile);
	}
	
	function MenuItemBilder($Array){
		$NotesMenuTemplate='<div class="menu_item_modul_notes_div"><a class="menu_item_modul_notes" href="'.$Array['href'].'">
		'.$Array['label'].'</a></div>';
	
		echo $NotesMenuTemplate;
	
	}
	
	
	
	function RenameOrPositionRegion(){
		
		$Data = array();
		$Data['label'] = 'Названия и порядок';
		$Data['href'] = '?page=administrator&action=regionmanager';
	
		return $Data;
	}
	
	function AddRegion(){
	
		$Data = array();
		$Data['label'] = 'Добавить новый регион';
		$Data['href'] = '?page=administrator&action=regionmanager';
	
		return $Data;
	}
	
	function AddSubRegion(){
	
		$Data = array();
		$Data['label'] = 'Добавить под категорию';
		$Data['href'] = '?page=administrator&action=regionmanager';
	
		return $Data;
	}
	
	
}