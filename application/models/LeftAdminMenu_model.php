<?php
class LeftAdminMenu_model{
	private $DataBaseHandle = null;
	
	function RenderView(){
		$IncludePathViewFile = 'application/views/LeftAdminMenu_view.php';
		if (file_exists($IncludePathViewFile)){
			ob_start();	
			
			$this->DataBaseHandle = DataBaseFunction::getInstans();
			
			require_once $IncludePathViewFile;
			
			return ob_get_clean();
			
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindView.$IncludePathViewFile);
	}
}
?>