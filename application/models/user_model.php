<?php
class user_model{
	function RenderView($ViewFile){
		$IncludePathViewFile = 'application/views/'.$ViewFile.'_view.php';
		
		if (file_exists($IncludePathViewFile)){
			ob_start();
				
			require_once $IncludePathViewFile;
			
			return ob_get_clean();
			
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindView.$IncludePathViewFile);
	}
	
	function GetUserRole($IdRole){

		$query = "SELECT nameRole FROM `users_roles` WHERE id='".$IdRole."'";
		
		$DataBaseHandle = DataBaseFunction::getInstans();
		
		$result = $DataBaseHandle->Query($query);
		$row = mysql_fetch_assoc($result);
		
		return $row['nameRole'];
	}
	
	function GetUserGenderLabel($Id){
		if ($Id==1) return 'Мужской';
		if ($Id==2) return 'Женский';
		return 'Неизвестно';
	}
}