<?php
class administrator_regionmanager_model{
	private $DataBaseHandle = null;
	private $OutputText=NULL;
	
	function RenderView($ViewFile){
			ob_start();

			$this->DataBaseHandle = DataBaseFunction::getInstans();
			$FC = FrontController::getInstans();
			switch ($FC->GetRequestUserParam('type'))
			{
				case 'edit':
					$IncludePathViewFile = self::Type_Edit($ViewFile);
					break;
				case 'up':
					$IncludePathViewFile = self::Type_Up($ViewFile);
					break;
				case 'down':
					$IncludePathViewFile = self::Type_Down($ViewFile);
					break;
				default:
					$IncludePathViewFile = self::Type_Default($ViewFile);
			}
			
			$OutputText = $this->OutputText;
			require_once $IncludePathViewFile;
			
			return ob_get_clean();
	}
	
	public function Region_getNamesRegion(){
		$resultArray = array();
		$result = $this->DataBaseHandle->Query('SELECT id,nameRegion,position,count_notes FROM  `regions` ORDER BY  `regions`.`position` ASC ');
		while ($row = mysql_fetch_array($result)){
			$resultArray[] = $row;
		}
		return $resultArray;
	}
	
	public function Region_getSubRegions($Idregions=0){
		$resultArray = array();
		$result =  $this->DataBaseHandle->Query("SELECT id,nameRegion,mainRegion,position,count_notes FROM  `subregions` WHERE `mainRegion`=$Idregions ORDER BY  `subregions`.`position` ASC ");
		while ($row = mysql_fetch_array($result)){
			$resultArray[] = $row;
		}
		return $resultArray;
	}
	
	public function Buid_TextBox($Id,$ValueBox,$Type){
		if ($Type=="s") $addText='<i class="fa fa-angle-double-right"></i>&nbsp;';
		
		$href = "/?page=administrator&action=regionmanager&idcell=".$Type.$Id."&type=";
		$Text = '<div class="admin-region-textedit">'.$addText.'<input type="text" readonly name="'.$Type.$Id.'" value="'.$ValueBox.'"><div class="admin-region-textedit-bar">
		<a href="'.$href.'edit" title="Изменить"><i class="fa fa-pencil-square-o fa-lg"></i></a>
		<a href="'.$href.'up" title="Переместить вверх"><i class="fa fa-level-up fa-lg"></i></a>
		<a href="'.$href.'down" title="Переместить вниз"><i class="fa fa-level-down fa-lg"></i></a>
		<a href="'.$href.'del" title="Удалить ячейку и все что внутри ячейки"><i class="fa fa-minus fa-lg"></i></a>
		</div></div>';
		return $Text;
	}
	
	public function Type_Default($ViewFile){
		return 'application/views/'.$ViewFile.'_regionmanager_view.php';
	}
	
	public function Type_Edit($ViewFile){
		return 'application/views/'.$ViewFile.'_regionmanager_edit_view.php';
	}
	
	public function Type_Up($ViewFile){
		return 'application/views/'.$ViewFile.'_regionmanager_view.php';
	}
	
	public function Type_Down($ViewFile){
		return 'application/views/'.$ViewFile.'_regionmanager_view.php';
	}
	
	
	
	
	
	
	
}