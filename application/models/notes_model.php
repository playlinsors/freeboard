<?php
class notes_model{
	private $DataBaseHandle = null;
	private $FilterData = array();
	private $DataPageOuterFilter = array ();
	
	function RenderView($ViewFile,$data=NULL){
		$IncludePathViewFile = 'application/views/'.$ViewFile.'_view.php';
		if ($data) $this->FilterData = $data;
		
		if (file_exists($IncludePathViewFile)){
			ob_start();
				
			$this->DataBaseHandle = DataBaseFunction::getInstans();

			require_once $IncludePathViewFile;
			
			return ob_get_clean();
			
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindView.$IncludePathViewFile);
	}
///////////////////////////////////////////////////////		PAGE FILTER   //////////////////////////////////////////////////////////////////////////////
	public function Notes_getArrayNotes_fabric($StartLimit,$EndLimit){
		$dataFilter = $this->FilterData[1];
		switch ($this->FilterData[0]) {
			case 'region':
				$this->DataPageOuterFilter['HeadText'] = $this->Notes_pageHeadText_region($dataFilter);
				$this->DataPageOuterFilter['Notes'] = $this->NotesArrayPage_product_region($StartLimit, $EndLimit,$dataFilter);
			break;
			
			case 'subregion':
				$this->DataPageOuterFilter['HeadText'] = $this->Notes_pageHeadText_subregion($dataFilter);
				$this->DataPageOuterFilter['Notes'] = $this->NotesArrayPage_product_subregion($StartLimit, $EndLimit,$dataFilter);
			break;
			
			case 'usernotes':
				$this->DataPageOuterFilter['HeadText'] = $this->Notes_pageHeadText_usernotes($dataFilter);
				$this->DataPageOuterFilter['Notes'] = $this->NotesArrayPage_product_usernotes($StartLimit, $EndLimit,$dataFilter);
			break;
			
			case 'mynotes':
				$dataFilter = $_SESSION['USERDATA_login'];
				$this->DataPageOuterFilter['HeadText'] = $this->Notes_pageHeadText_usernotes($dataFilter);
				$this->DataPageOuterFilter['Notes'] = $this->NotesArrayPage_product_usernotes($StartLimit, $EndLimit,$dataFilter);
			break;
			
			default:
				$this->DataPageOuterFilter['HeadText'] = $this->Notes_pageHeadText_none();
				$this->DataPageOuterFilter['Notes'] = $this->NotesArrayPage_product_none($StartLimit, $EndLimit);
		}
		return $this->DataPageOuterFilter['Notes'];
	}
	
	/**
	 * Выборка продукции без фильтров
	 * @return multitype:multitype: 
	 */
	public function NotesArrayPage_product_none($StartLimit,$EndLimit){
		$resultArray = array();
		$query = "SELECT
		notes.id,notes.typenotes,notes.miniPhotoPath,notes.Tinyinfo,notes.idLogin,notes.date,users.login
		FROM `notes`
		LEFT JOIN users ON notes.idLogin=users.id
		WHERE Status=1
		ORDER BY notes.id DESC
		LIMIT $StartLimit,$EndLimit";
		
		$result = $this->DataBaseHandle->Query($query);
		while ($row = mysql_fetch_assoc($result)){
		$resultArray[] = $row;
		}
		return $resultArray;
	}
	
	/**
	 * Выборка продукции по региону
	 * @return multitype:multitype:
	 */
	public function NotesArrayPage_product_region($StartLimit,$EndLimit,$dataFilter){
		$resultArray = array();
		$dataFilter = abs($dataFilter)*1;
		$query = "SELECT
		notes.id,notes.typenotes,notes.miniPhotoPath,notes.Tinyinfo,notes.idLogin,notes.date,users.login
		FROM `notes`
		LEFT JOIN users ON notes.idLogin=users.id
		WHERE Status=1
		AND notes.idRegion = '".$dataFilter."'
		ORDER BY notes.id DESC
		LIMIT $StartLimit,$EndLimit";
	
		$result = $this->DataBaseHandle->Query($query);
		while ($row = mysql_fetch_assoc($result)){
			$resultArray[] = $row;
		}
		return $resultArray;
	}
	
	/**
	 * Выборка продукции по субрегиону
	 * @return multitype:multitype:
	 */
	public function NotesArrayPage_product_subregion($StartLimit,$EndLimit,$dataFilter){
		$dataFilter = abs($dataFilter)*1;
		$resultArray = array();
		$query = "SELECT
		notes.id,notes.typenotes,notes.miniPhotoPath,notes.Tinyinfo,notes.idLogin,notes.date,users.login
		FROM `notes`
		LEFT JOIN users ON notes.idLogin=users.id
		WHERE Status=1
		AND notes.idSubregion = '".$dataFilter."'
		ORDER BY notes.id DESC
		LIMIT $StartLimit,$EndLimit";
	
		$result = $this->DataBaseHandle->Query($query);
		while ($row = mysql_fetch_assoc($result)){
			$resultArray[] = $row;
		}
		return $resultArray;
	}

	/**
	 * Выборка продукции по пользователю
	 * @return multitype:multitype:
	 */
	public function NotesArrayPage_product_usernotes($StartLimit,$EndLimit,$dataFilter){
		$dataFilter = trim(mysql_escape_string($dataFilter));
		$query = "SELECT id FROM users WHERE users.login = '$dataFilter';";
		$result = $this->DataBaseHandle->Query($query);
		$row = mysql_fetch_assoc($result);
		if ($row['id'] != 0)
		{
			$resultArray = array();
			$query = "SELECT
						notes.id,notes.typenotes,notes.miniPhotoPath,notes.Tinyinfo,notes.idLogin,notes.date,users.login
					FROM `notes`
					LEFT JOIN users ON notes.idLogin=users.id
					WHERE Status=1
					AND notes.idLogin = '".$row['id']."'
					ORDER BY notes.id DESC
					LIMIT $StartLimit,$EndLimit";
			
			$result = $this->DataBaseHandle->Query($query);
			while ($row = mysql_fetch_assoc($result)){
				$resultArray[] = $row;
			}
			return $resultArray;
				
		}
		return null;
		
	}
////////////////////////////////////////////////////////////
	/**
	 * Возвращает имя страницы
	 * @return multitype:multitype:
	 */
	public function Notes_getNameHeaderPage(){
		return $this->DataPageOuterFilter['HeadText'];
	}
	
	/**
	 * Записывает Верхний заголовок
	 * @return multitype:multitype:
	 */
	public function Notes_pageHeadText_none(){
		return 'Все обьявления';
	}
	
	/**
	 * Записывает Верхний заголовок
	 * @return multitype:multitype:
	 */
	public function Notes_pageHeadText_region($id){
		$id = abs($id)*1;
		$query = "SELECT nameRegion FROM regions WHERE regions.id = '$id';";
		$result = $this->DataBaseHandle->Query($query);
		$row = mysql_fetch_assoc($result);
		$Text = "Сортировать по региону : ".$row['nameRegion'];
		return $Text;
	}
	
	/**
	 * Записывает Верхний заголовок
	 * @return multitype:multitype:
	 */
	public function Notes_pageHeadText_subregion($id){
		$id = abs($id)*1;
		$query = "SELECT nameRegion FROM subregions WHERE subregions.id = '$id';";
		$result = $this->DataBaseHandle->Query($query);
		$row = mysql_fetch_assoc($result);
		$Text = "Сортировать по : ".$row['nameRegion'];
		return $Text;
	}
	
	/**
	 * Записывает Верхний заголовок
	 * @return multitype:multitype:
	 */
	public function Notes_pageHeadText_usernotes($id){
		$id = trim(mysql_escape_string($id));
		$query = "SELECT id FROM users WHERE users.login = '$id';";
		$result = $this->DataBaseHandle->Query($query);
		$row = mysql_fetch_assoc($result);
		if ($row['id'] != 0)
		{
			$Text = "Обьявления пользователя : ".$id;
			return $Text;
			
		} 
		return 'Пользователь не найден';
	}
	
	
////////////////////////////////////////////////////////////////////////////
	public function Notes_PagePartFilter(){
		
		if (($_GET['part']!=null)&& ($_GET['part']>0)){
			$NotesPart = abs($_GET['part']*1);
		} else 
		{
			$NotesPart = 1;
		}
		return $NotesPart; 
	}
	
	
	public function Notes_getType($id){
		$Text = "неизвестно";
		if ($id==1) $Text = LangTextSring::$TextNotesTypeSell;
		if ($id==2) $Text = LangTextSring::$TextNotesTypeBuy;
		return $Text;
	}
		
	public function Notes_getColorType($id){
		$Color = "000000";
		if ($id==1) $Color = 'green';
		if ($id==2) $Color = 'blue';
		return $Color;
	}
	
	public function Notes_getUserName($id){
		
	}
	
}