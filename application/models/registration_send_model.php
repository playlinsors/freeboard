<?php
class registration_send_model{
	function RenderView($ViewFile){
		$IncludePathViewFile = 'application/views/'.$ViewFile.'_view.php';
		if (file_exists($IncludePathViewFile)){
			ob_start();
			$registration_errorText = null;
			
			if ($_POST['captha']==$_SESSION['captcha_keystring'])
			{
				if (($_POST['login']!=null) && ($_POST['password']!=null) && ($_POST['email']!=null))
				{
					if ($_POST['password']==$_POST['password2']){
						if (strlen($_POST['password'])>5){
						$DB = DataBaseFunction::getInstans();
						$result = $DB->Query('SELECT login FROM `users` WHERE `login`="'.$this->ClearData(strtolower($_POST['login'])).'"');
						$row = mysql_fetch_array($result);
							if ($row[0]==null)
							{
								if ( (($_POST['birthday']==null) && ($_POST['birthmouth']==null) && ($_POST['birthyear']==null)) ||
									 (($_POST['birthday']<=31) && ($_POST['birthday']>=1) && 
									 ($_POST['birthmouth']<=12) && ($_POST['birthmouth']>=1) && 
									 ($_POST['birthyear']>1900) && ($_POST['birthyear']<3000) ))
								{
								if ($_POST['rule']=='yes'){
									if( preg_match("/^[-_a-zA-Z0-9]{3,20}$/",$_POST['login'])){
										if (preg_match("/.+@.+\..+/i",$_POST['email'])){
											if ($_POST['gender']!=null){
												
												$returnText = $this->SaveUser();
												if ($returnText==null){
												$outputText = "Вы успешно зарегистрировались на сайте , для активации аккаунта 
												необходимо подтвердить его через свой e-mail( 
												<strong>".$this->ClearData($_POST['email'])."</strong>)";
												} else $outputText = $returnText;
											} else $registration_errorText = "Укажите свой пол";
										} else $registration_errorText="Не корректный email";	
									} else $registration_errorText = "Логин должен быть от 3-10 символов, допускается латинские буквы,цифры и знак _ ";
								} else $registration_errorText = "Прочтите правила, пожалуйста";
								} else $registration_errorText = "Дата рождения должна быть в формате дд.мм.гггг";
							} else $registration_errorText = "Ошибка,введенный логин уже существует.";
						} else $registration_errorText="Ошибка, придумайте нормальный пароль, который больше 5 символов";
					} else $registration_errorText = "Ошибка,введенные пароли не совпадают";
				} else $registration_errorText ="Ошибка, не заполнены основные поля";
			} else $registration_errorText ="Ошибка, неправильно введен проверочный код";
			
			if ($registration_errorText==null){
				$IncludePathViewFile = 'application/views/INFO_MESSAGE_view.php'; 
			}  

			require_once $IncludePathViewFile;
			
			return ob_get_clean();
			
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindView.$IncludePathViewFile);
	}
	
	/**
	 * Проверка основных данных и Сохранение в ссесию
	 */
	function ClearData($text){
		$text = trim($text);
		$text = htmlspecialchars($text);
		$text = mysql_escape_string($text);
		return $text;
	}
	
	function GetSumUserBirthDay(){
		$result = $this->ClearData($_POST['birthday']).".";
		$result .= $this->ClearData($_POST['birthmouth']).".";
		$result .= $this->ClearData($_POST['birthyear']);
		if ($result=='..') return null;
		return $result;
	}
	
	function SendEmail($ComfirmHash){	
		$to      = $_POST['email'];
		$subject = 'Регистрация на сайте '.LangTextSring::$TextSiteName ;
		$message = 'Приветствуем Вас, '.$_POST['login']." <br><br>".
              "Для активации аккаунта, пожалуйста, нажмите на ссылку ниже: <br><br>".
              "http://".$_SERVER['HTTP_HOST']."/?page=registration&action=confirmemail&value=".$ComfirmHash." <br><br>".
              "С уважением, администрация сайта ".LangTextSring::$TextSiteName."<br><br>";
        
		$headers = 	"MIME-Version: 1.0\r\n";
		$headers .= 'Content-Type: text/html;charset="utf-8"'."\r\n";  
		$headers .= 'From: '.$GLOBALS['ADMINISTRATOR_EMAIL']."\r\n";
		$headers .= "X-Mailer: My Send E-mail \r\n";
			
		return  mail($to, $subject, $message, $headers) ;
	}
	
	function SaveUser(){
		
		$genderFiltered = $this->ClearData($_POST['gender']);
		$genderFiltered = ($genderFiltered==1) ? '1':'2';
		
		$query = "INSERT INTO  `users` (
		`id` ,
		`login` ,
		`password` ,
		`name` ,
		`first name` ,
		`last name` ,
		`gender`,
		`email` ,
		`Sity` ,
		`Organization` ,
		`Telephone`,
		`info` ,
		`birthday` ,
		`DataReg`,
		`avatar` ,
		`access` ,
		`confirmemail`
		)	VALUES (
		NULL ,
		'".$this->ClearData($_POST['login'])."',
		'".md5($this->ClearData($_POST['password']))."',
		'".$this->ClearData($_POST['name'])."',
		'".$this->ClearData($_POST['firstname'])."',
		'".$this->ClearData($_POST['lastname'])."',
		'".$genderFiltered."',
		'".$this->ClearData($_POST['email'])."',
		'".$this->ClearData($_POST['sity'])."',
		'".$this->ClearData($_POST['organization'])."',
		'".$this->ClearData($_POST['Telephone'])."',		
		'".$this->ClearData($_POST['about'])."',
		'".$this->GetSumUserBirthDay()."',
		'".date("d.m.Y")."',
		'',  
		'3',  
		'0');";
		
		$DB = DataBaseFunction::getInstans();
		$result = $DB->Query($query);
		if ($result==false) FrontController::GeneratePageFatalError("Что то произошло с базой данных");
		
		$result = $DB->Query('SELECT id FROM `users` WHERE `login`="'.$this->ClearData(strtolower($_POST['login'])).'"');
		$row = mysql_fetch_array($result);
		
		$UserId = $row[0];
		$ComfirmHash = md5(rand(10000,99999));
		$Expirientsdate = date("Y-m-d", (time()+3600*24*3));
		
		$query = "INSERT INTO `temp_confirm_info` (
		`id`, 
		`userId`,
		`comfirmhash`,
		`expirientsdate`
		) VALUES (
		NULL, 
		'".$UserId."', 
		'".$ComfirmHash."', 
		'".$Expirientsdate."' 
		);";
		
		$result = $DB->Query($query);
		if ($result==false) FrontController::GeneratePageFatalError("Что то произошло с базой данных");
		
		if ($this->SendEmail($ComfirmHash)==false) return "Вы зарегистрированы, но не удалось отправить email, обратитесь к администратору.";
		
		return NULL;
	}
}