<?php
class contacts_model{
	function RenderView($ViewFile){
		$IncludePathViewFile = 'application/views/'.$ViewFile.'_view.php';
		
		if (file_exists($IncludePathViewFile)){
			ob_start();
			
			if ($_SESSION['USERDATA_id']==null){
				$Contact_SendName = 'Гость';
				$Contact_Sendemail= 'guest@mail.ru';
			} else {
				$Contact_SendName =$_SESSION['USERDATA_name'];
				$Contact_Sendemail = $_SESSION['USERDATA_email'];
			}
			
			require_once $IncludePathViewFile;
			
			return ob_get_clean();
			
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindView.$IncludePathViewFile);
	}
}