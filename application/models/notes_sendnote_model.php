<?php
class notes_sendnote_model{	
	private $DataBaseHandle = null;
	private $ListLoadFiles;
	
	function RenderView($ViewFile){
		$IncludePathViewFile = 'application/views/INFO_MESSAGE_view.php';
		
		if (file_exists($IncludePathViewFile)){
			ob_start();
			$this->DataBaseHandle = DataBaseFunction::getInstans();
			$note_errorText = null;
			
			if ($_POST['captha']!=$_SESSION['captcha_keystring']) {
				$note_errorText="Неправильно введены цифры с картинки"; 
			} else {
				$_SESSION['captcha_keystring']=1; // от постов дубликатов
				
				if (($_SESSION['USERDATA_id']==null) && ($_POST['name']==null) && ($_POST['sity']==null) && ($_POST['telephone']==null) && ($_POST['email']==null)){
					$note_errorText="Пожалуйста, заполните контактные данные";
				}
				
				if (strlen($_POST['submit_name'])>200) $note_errorText = "Заголовок не должен превышать 100 символов "; 
				if (strlen($_POST['submit_text'])>2000) $note_errorText = "Текст обьявления не должен превышать 1000 символов -";
				if ((strlen($_POST['name'])>50) || (strlen($_POST['sity'])>50) || (strlen($_POST['telephone'])>50) || (strlen($_POST['email'])>50) ) {
				$note_errorText = "Поля 'имя','город','телефон','email' не должны превышать размера в 50 символов ";
				}
				
				if (($_POST['type']!="buy") && ($_POST['type']!="sell")){
					$note_errorText="Выберите тип операции";
				}
				if (($_POST['submit_name']==null) && ($_POST['submit_text']==null)) {
					$note_errorText="Введите заголовок обьявления и сам текст обьявления";
				}
				if (($_POST['Catalog']==0) && (!is_int($_POST['Catalog']))){
					$note_errorText="Выберите корректный регион";
				}
			    for ($i = 1; $i <= 4; $i++) {
			    	if ($_FILES['img'.$i]['error']==4) continue;
			    	if ($_FILES['img'.$i]['error']==1){
			    		$note_errorText="Некоторые файлы не загрузились, проверьте их размер и попрубуйте еще раз";
			    	}
			    	if ($_FILES['img'.$i]['size']>'2000000'){
			    		Echo $_FILES['img'.$i]['size'];	
			    		$note_errorText="Проблемы с загрузкой файлов";
			    	}
			    	$exp = explode('/',$_FILES['img'.$i][type]);
			    	if ($exp[0]!='image') {
			    			$note_errorText="Некоторые файлы не являются изображением";
			    	}
			    }
			}
			
			// Если ошибка то грузим обратно форму, если нет то просто выводим сообщение
			if ($note_errorText!=null) {
				$IncludePathViewFile = 'application/views/notes_addnote_view.php';
				
			} else {
				
				$outputText="Ваше обявление отправлено";
				$this->CopyFileImage();
				$this->Save_notes();
			}
	
			require_once $IncludePathViewFile;
			
			return ob_get_clean();
			
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindView.$IncludePathViewFile);
	}

function Save_notes(){
// Region из subregion
	$query="SELECT mainRegion FROM `subregions` WHERE id='".$this->ClearData($_POST['Catalog'])."'";
	$result = $this->DataBaseHandle->Query($query);
	$row = mysql_fetch_array($result);
	$RegionIndex = $row[0];
	
	$TypeNotes = ($this->ClearData($_POST['type'])=='buy') ? '2' : '1';
	
	if ($_SESSION['USERDATA_id']==null){
		$UserId = 0;
		$UserName = $this->ClearData($_POST['name']);
		$UserSity = $this->ClearData($_POST['sity']);
		$UserTelephone = $this->ClearData($_POST['telephone']);
		$UserEmail = $this->ClearData($_POST['email']);
	} else {
		$UserId = $_SESSION['USERDATA_id'];
		$UserName = $_SESSION['USERDATA_name'];
		$UserSity = $_SESSION['USERDATA_Sity'];
		$UserTelephone = $_SESSION['USERDATA_Telephone'];
		$UserEmail = $_SESSION['USERDATA_email'];
	}
	// центральный запрос
	$query = "INSERT INTO `notes` (
	`id`, `IdRegion`, `idSubregion`,
	`typenotes`, `miniPhotoPath`,`Tinyinfo`,
	`infodata`, 
	`idLogin`, `Name`, `email`,
	`telephone`, `sity`, `Status`,
	`photoPath1`, `photoPath2`, `photoPath3`,
	`photoPath4`,`date`) VALUES (
	 NULL, '".$RegionIndex."', '".$this->ClearData($_POST['Catalog'])."',
	'".$TypeNotes."', '".$this->ListLoadFiles[0]."','".$this->ClearData($_POST['submit_name'])."', 
	'".$this->ClearData($_POST['submit_text'])."','".$UserId."', '".$UserName."',
	'".$UserEmail."','".$UserTelephone."', '".$UserSity."',
	'1','".$this->ListLoadFiles[1]."', '".$this->ListLoadFiles[2]."', '".$this->ListLoadFiles[3]."',
	 '".$this->ListLoadFiles[4]."','".date("Y-m-d")."');";
	
	$result = $this->DataBaseHandle->Query($query);
	// Добавление к регионом параметров
	$query="SELECT `count_notes` FROM `regions` WHERE id='".$RegionIndex."';";
	$result = $this->DataBaseHandle->Query($query);
	$row = mysql_fetch_array($result);
	$CountNotesInRegion = $row[0];
	$CountNotesInRegion++;
	$query="UPDATE `regions` SET `count_notes` = '".$CountNotesInRegion."' WHERE `id` ='".$RegionIndex."';";
	$result = $this->DataBaseHandle->Query($query);
	
	// Суб регион
	$query="SELECT `count_notes` FROM `subregions` WHERE id='".$this->ClearData($_POST['Catalog'])."';";
	$result = $this->DataBaseHandle->Query($query);
	$row = mysql_fetch_array($result);
	$CountNotesInsubRegion = $row[0];
	$CountNotesInsubRegion++;
	$query="UPDATE `subregions` SET `count_notes` = '".$CountNotesInsubRegion."' WHERE `id` ='".$this->ClearData($_POST['Catalog'])."';";
	$result = $this->DataBaseHandle->Query($query);
	
}

function CopyFileImage(){
	$uploaddir = $GLOBALS['NotesSettings_pathtoImage'];
	$FlagCreateSmailImage = true;
	
	$query="SELECT id FROM `notes` ORDER BY `id` DESC";
	$result = $this->DataBaseHandle->Query($query);
	$row = mysql_fetch_array($result);
	$indexImage = $row[0];
	$countIndex=0;
	
	for ($i = 1; $i <= 4; $i++) {
		if ($_FILES['img'.$i]['error']==0){
			$countIndex +=1;
			$exp = explode('/',$_FILES['img'.$i][type]);
			$uploadfile= "noteImage_".$indexImage."_".$countIndex.".".$exp[1];
			
			if (!copy( $_FILES['img'.$i]['tmp_name'], $uploaddir.$uploadfile)){
				echo "Файлы не были скопированы, сообщите администратору об ошибке";
			} else {
				if ($FlagCreateSmailImage){
					$SmallImFilename = $uploaddir.'smallImage_'.$indexImage.".".$exp[1];
					$this->img_resize($uploaddir.$uploadfile,$SmallImFilename ,64, 64);
					$this->ListLoadFiles[] = 'smallImage_'.$indexImage.".".$exp[1];
					$FlagCreateSmailImage=false;
					}
				$this->ListLoadFiles[] = $uploadfile;
			}
		}
	}
}	

function ClearData($text){
		$text = trim($text);
		$text = htmlspecialchars($text);
		$text = mysql_escape_string($text);
		return $text;
	}

/***********************************************************************************
Функция img_resize(): генерация thumbnails
Параметры:
  $src             - имя исходного файла
  $dest            - имя генерируемого файла
  $width, $height  - ширина и высота генерируемого изображения, в пикселях
Необязательные параметры:
  $rgb             - цвет фона, по умолчанию - белый
  $quality         - качество генерируемого JPEG, по умолчанию - максимальное (100)
***********************************************************************************/
function img_resize($src, $dest, $width, $height, $rgb=0xFFFFFF, $quality=100)
{
  if (!file_exists($src)) return false;
 
  $size = getimagesize($src);
 
  if ($size === false) return false;
 
  // Определяем исходный формат по MIME-информации, предоставленной
  // функцией getimagesize, и выбираем соответствующую формату
  // imagecreatefrom-функцию.
  $format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
  $icfunc = "imagecreatefrom" . $format;
  if (!function_exists($icfunc)) return false;
 
  $x_ratio = $width / $size[0];
  $y_ratio = $height / $size[1];
 
  $ratio       = min($x_ratio, $y_ratio);
  $use_x_ratio = ($x_ratio == $ratio);
 
  $new_width   = $use_x_ratio  ? $width  : floor($size[0] * $ratio);
  $new_height  = !$use_x_ratio ? $height : floor($size[1] * $ratio);
  $new_left    = $use_x_ratio  ? 0 : floor(($width - $new_width) / 2);
  $new_top     = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);
 
  $isrc = $icfunc($src);
  $idest = imagecreatetruecolor($width, $height);
 
  imagefill($idest, 0, 0, $rgb);
  imagecopyresampled($idest, $isrc, $new_left, $new_top, 0, 0,
    $new_width, $new_height, $size[0], $size[1]);
 
  imagejpeg($idest, $dest, $quality);
 
  imagedestroy($isrc);
  imagedestroy($idest);
 
  return true;
 
}	
	
///////// КОСТЫЛЬ! notes_addnote_model duplicate
	public function Region_getNamesRegion(){
		$resultArray = array();
		$result = $this->DataBaseHandle->Query('SELECT id,nameRegion,position FROM  `regions` ORDER BY  `regions`.`position` ASC ');
		 while ($row = mysql_fetch_array($result)){
					$resultArray[] = $row;
				}
		return $resultArray;
	}
	
	public function Region_getSubRegions($Idregions=0){
		$resultArray = array();
		$result =  $this->DataBaseHandle->Query("SELECT * FROM  `subregions` WHERE `mainRegion`=$Idregions ORDER BY  `subregions`.`position` ASC ");
		while ($row = mysql_fetch_array($result)){
					$resultArray[] = $row;
				}
		return $resultArray;
	}
/////////////////////////////////////////////////////
}