<?php
class LeftTopMenu_model{	
	function RenderView(){
		$IncludePathViewFile = 'application/views/LeftTopMenu_view.php';
		if (file_exists($IncludePathViewFile)){
			ob_start();	
			
			require_once $IncludePathViewFile;
			
			return ob_get_clean();
			
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindView.$IncludePathViewFile);
	}
	
	function MenuItemBilder($Array){
		$NotesMenuTemplate='<div class="menu_item_modul_notes_div"><a class="menu_item_modul_notes" href="'.$Array['href'].'">
		'.$Array['label'].'</a></div>';
	
		echo $NotesMenuTemplate;
	
	}
	
	function FlagShowMyNotes(){
		if ($_SESSION['USERDATA_id']!=0) 
			return true;
		return  false;
	}
	
	function DataShowMyNotes(){
		$Data = array();
		$Data['label'] = 'Мои объявления';
		$Data['href'] = '/?page=notes&action=mynotes';
		
		return $Data;
	}
	
	function FlagDeleteNote(){
		if ($_SESSION['USERDATA_id']!=0)
		{
			$FC = FrontController::getInstans();
			if ($FC->GetAction()=='showaction')
			{ 
				$ID = $_GET['id'];
				if (($ID<0) || ($ID==null)) return false;
				$ID = $ID*1;
				
				if ($_SESSION['USERDATA_ACCESS']['AccessOnModerationPosts']) return true;
				
				$DataBaseHandle = $this->DataBaseHandle = DataBaseFunction::getInstans();
				$query = "SELECT notes.idLogin 
						  FROM  notes
						  WHERE notes.id = '$ID';";
				
				$result = $this->DataBaseHandle->Query($query);
				$row = mysql_fetch_assoc($result);
				if ($row['idLogin']==$_SESSION['USERDATA_id']) return true;
			}
		}
		return  false;
	}
	
	function DeleteNote(){
		$ID = $_GET['id'];
		if (($ID<0) || ($ID==null)) return false;
		$ID = $ID*1;
		
		$Data = array();
		$Data['label'] = 'Удалить обьявление';
		$Data['href'] = '/?page=notes&action=deletenote&id='.$ID;
	
		return $Data;
	}
	
}