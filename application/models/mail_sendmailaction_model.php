<?php
class mail_sendmailaction_model{
	function RenderView($ViewFile){
		ob_start();

		$Contact_SendFromName = $this->ClearData($_POST['name']);
		$Contact_SendFromEmail = $this->ClearData($_POST['mail']);
		$Contact_SendToName = $this->ClearData($_POST['user']);
		$Other_noteId = abs($this->ClearData($_POST['noteId']) * 1);
		$Contact_ActionAs = $this->ClearData($_POST['actionAs']);
		
		if ($_POST['kapcha']!=$_SESSION['captcha_keystring']) $statusError = "Неправильный ввод с проверочной картинки ";
		if (isset($_POST['name']) && $_POST['name'] == "") $statusError = "Не введено имя";
		if (isset($_POST['mail']) && $_POST['mail'] == "") $statusError = "Не введен e-mail";
		if (isset($_POST['text']) && $_POST['text'] == "") $statusError = "Напишите текст сообщения";
		if (isset($_POST['kapcha']) && ($_POST['kapcha'] == "")) $statusError = "Ошибка: Не введена капча";
		
		if ($statusError=="")
		{
			$_SESSION['captcha_keystring'] = rand(1, 999999);
			$StatusSend = false;
			
			switch ($Contact_ActionAs) {
				case 'simple':
					$Contact_SendToEmail = $this->getEmailFromUser($Contact_SendToName);
					$StatusSend = $this->SimpleActionDate($Contact_SendFromName,$Contact_SendFromEmail,$Contact_SendToName,$Contact_SendToEmail);
					break;
				case 'NoteSend':
					$Contact_SendToEmail = $this->getEmailFromIdNote($Other_noteId);
					$StatusSend = $this->NoteActionDate($Other_noteId,$Contact_SendFromName,$Contact_SendFromEmail,$Contact_SendToName,$Contact_SendToEmail);
					break;
				default:
					$StatusSend = $this->SimpleActionDate($Contact_SendFromName,$Contact_SendFromEmail,$Contact_SendToName,$Contact_SendToEmail);
					
			}
			if ($StatusSend==true){
					$outputText = "<strong>Спасибо!</strong><br> Ваше сообщение было отправленно пользователю ".$Contact_SendToName;
			} else 	$outputText = "<strong>Неприятность</strong><br> Ваше сообщение НЕ было отправленно,пожалуйста,сообщите администратору сайта. ";
		
			$IncludePathViewFile = 'application/views/INFO_MESSAGE_view.php';
		} else {
			$IncludePathViewFile = 'application/views/'.$ViewFile.'_view.php';	
		}
			require_once $IncludePathViewFile;
			
			return ob_get_clean();			
	}
	
	function ClearData($text){
		$text = trim($text);
		$text = htmlspecialchars($text);
		$text = mysql_escape_string($text);
		return $text;
	}
	
	function getEmailFromUser($UserName){

		$DB = DataBaseFunction::getInstans();
		$query = "SELECT email FROM users WHERE login='".$UserName."'";
		$result = $DB->Query($query);
		$row = mysql_fetch_array($result);
		return $row[0];
	}
	
	function getEmailFromIdNote($IdNote){

		$DB = DataBaseFunction::getInstans();
		$query = "SELECT email FROM notes WHERE id='".$IdNote."'";
		$result = $DB->Query($query);
		$row = mysql_fetch_array($result);
		return $row[0];
	}
	
	function SimpleActionDate($FromName,$FromEmail,$ToName,$ToEmail){
		if ($_SESSION['USERDATA_id']==null) { 
				$message = 'Личное сообщение от гостя по имени <strong>'.$FromName."</strong><hr />";
			}else {
				$message = 'Личное сообщение от пользователя <strong>'.$_SESSION['USERDATA_login']."</strong><hr />";
			}
			
			$message .= $this->ClearData($_POST['text']);
			$message .= "<hr />";
			
			return $this->SendMail($ToEmail, $FromEmail, $message);
	}
	
	function NoteActionDate($noteId,$FromName,$FromEmail,$ToName,$ToEmail){
		
		if ($ToEmail==null) return 0;
		
		$message = 'Сообщения по опубликованному <a href="http://'.$_SERVER['HTTP_HOST'].'/?page=notes&action=show&id='.$noteId.'">обьявлению</a><hr /><br />';
		$message .= $this->ClearData($_POST['text']);
		$message .= "<br /><br /><hr />";
		
		if ($_SESSION['USERDATA_id']==null){
			$addedText ="Написал гость с именем $FromName";
		} else {
			$addedText ='Написал пользователь с ником <a href="http://'.$_SERVER['HTTP_HOST'].'/?page=userinfo&id='.$_SESSION['USERDATA_id'].'">'.$_SESSION['USERDATA_login'].'</a>';
		}
		
		$message .=$addedText;
			
		return $this->SendMail($ToEmail, $FromEmail, $message);
	}
	
	function SendMail($WhereUserEmail,$FromUserEmail,$Message){
			
			$subject = 'Сообщение с сайта '.LangTextSring::$TextSiteName ;
			
			$headers = 	"MIME-Version: 1.0\r\n";
			$headers .= 'Content-Type: text/html;charset="utf-8"'."\r\n";
			$headers .= "From: ".$FromUserEmail."\r\n";
			$headers .= "X-Mailer: My Send E-mail \r\n";
			
			return mail($WhereUserEmail, $subject, $Message, $headers);
	}
	
	
	
	
}