<?php
class BarStatusUserRegistration_model{
	function RenderView(){
		$IncludePathViewFile = 'application/views/BarStatusUserRegistration_view.php';
		if (file_exists($IncludePathViewFile)){
			ob_start();	
			
			require_once $IncludePathViewFile;
			return ob_get_clean();
			
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindView.$IncludePathViewFile);
	}
}