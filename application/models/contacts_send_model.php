<?php
class contacts_send_model{
	function RenderView($ViewFile){
		ob_start();

		$Contact_SendName = $_POST['name'];
		$Contact_Sendemail = $_POST['mail'];
		
		if ($_POST['kapcha']!=$_SESSION['captcha_keystring']) $statusError = "Неправильный ввод с проверочной картинки ";
		if (isset($_POST['name']) && $_POST['name'] == "") $statusError = "Не введено имя";
		if (isset($_POST['mail']) && $_POST['mail'] == "") $statusError = "Не введен e-mail";
		if (isset($_POST['text']) && $_POST['text'] == "") $statusError = "Напишите текст сообщения";
		if (isset($_POST['kapcha']) && ($_POST['kapcha'] == "")) $statusError = "Ошибка: Не введена капча";
		
		if ($statusError=="")
		{
			$_SESSION['captcha_keystring'] = rand(1, 999999);
			
			$to      = $GLOBALS['ADMINISTRATOR_EMAIL'];
			$subject = 'Отзыв c '.LangTextSring::$TextSiteName ;
			if ($_SESSION['USERDATA_id']==null) { 
				$message = 'Отзыв от гостя по имени <b>'.$_POST['name']."</b><hr />";
			}else {
				$message = 'Отзыв от пользователя <b>'.$_SESSION['USERDATA_login']."</b><hr />";
			}
			
			$message .= htmlspecialchars($_POST['text']); 
			
			$headers = 	"MIME-Version: 1.0\r\n";
			$headers .= 'Content-Type: text/html;charset="utf-8"'."\r\n";
			$headers .= "From: ".$_POST['mail']."\r\n";
			$headers .= "X-Mailer: My Send E-mail \r\n";
			
			mail($to, $subject, $message, $headers);	
			$outputText = "<strong>Спасибо!</strong><br> Ваше сообщение было отправленно, в ближайщие время мы свяжемся с вами.";
			
			$IncludePathViewFile = 'application/views/INFO_MESSAGE_view.php';
		} else {
			$IncludePathViewFile = 'application/views/'.$ViewFile.'_view.php';	
		}
			require_once $IncludePathViewFile;
			
			return ob_get_clean();			
	}
}