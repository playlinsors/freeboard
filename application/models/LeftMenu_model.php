<?php
class LeftMenu_model{
	private $DataBaseHandle = null;
	
	function RenderView(){
		$IncludePathViewFile = 'application/views/LeftMenu_view.php';
		if (file_exists($IncludePathViewFile)){
			ob_start();	
			
			$this->DataBaseHandle = DataBaseFunction::getInstans();
			
			require_once $IncludePathViewFile;
			
			return ob_get_clean();
			
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindView.$IncludePathViewFile);
	}
	
	public function Region_getNamesRegion(){
		$resultArray = array();
		$result = $this->DataBaseHandle->Query('SELECT id,nameRegion,position,count_notes FROM  `regions` ORDER BY  `regions`.`position` ASC ');
		 while ($row = mysql_fetch_array($result)){
					$resultArray[] = $row;
				}
		return $resultArray;
	}
	
	public function Region_getSubRegions($Idregions=0){
		$resultArray = array();
		$result =  $this->DataBaseHandle->Query("SELECT id,nameRegion,mainRegion,position,count_notes FROM  `subregions` WHERE `mainRegion`=$Idregions ORDER BY  `subregions`.`position` ASC ");
		while ($row = mysql_fetch_array($result)){
					$resultArray[] = $row;
				}
		return $resultArray;
	}
	
	public function GetNumberOrZero($i){
		if ($i==null) return '0';
		return "<i>".$i."</i>";
	}
}