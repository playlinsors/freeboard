<?php
class find_model{
	function RenderView($ViewFile){
		$IncludePathViewFile = 'application/views/'.$ViewFile.'_view.php';
		
		if (file_exists($IncludePathViewFile)){
			ob_start();
				
			require_once $IncludePathViewFile;
			
			return ob_get_clean();
			
		} else FrontController::GeneratePageFatalError(LangTextSring::$ErrorFindView.$IncludePathViewFile);
	}
}