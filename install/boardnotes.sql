-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Фев 12 2014 г., 14:19
-- Версия сервера: 5.5.35-log
-- Версия PHP: 5.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `boardnotes`
--

-- --------------------------------------------------------

--
-- Структура таблицы `headermenu`
--

CREATE TABLE IF NOT EXISTS `headermenu` (
  `id` int(11) NOT NULL COMMENT 'Index',
  `Label` varchar(40) NOT NULL COMMENT 'Надпись',
  `Link` varchar(50) NOT NULL COMMENT 'Ссылка на контроллер',
  `offset` int(11) NOT NULL COMMENT 'Сдвиг',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Настройки верхнего меню (НЕ работает)';

--
-- Дамп данных таблицы `headermenu`
--

INSERT INTO `headermenu` (`id`, `Label`, `Link`, `offset`) VALUES
(1, 'Главная', '/', 0),
(2, 'Обьявления', '/?page=notes', 0),
(3, 'Контакты', '/?page=contacts', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `notes`
--

CREATE TABLE IF NOT EXISTS `notes` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `IdRegion` int(11) NOT NULL,
  `idSubregion` int(11) NOT NULL,
  `typenotes` int(11) NOT NULL,
  `miniPhotoPath` varchar(50) NOT NULL,
  `Tinyinfo` text NOT NULL,
  `infodata` text NOT NULL,
  `idLogin` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telephone` varchar(50) NOT NULL,
  `sity` varchar(50) NOT NULL,
  `Status` int(10) NOT NULL,
  `photoPath1` varchar(40) NOT NULL,
  `photoPath2` varchar(40) NOT NULL,
  `photoPath3` varchar(40) NOT NULL,
  `photoPath4` varchar(40) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Главная таблица с обьявлениями' AUTO_INCREMENT=23 ;

--
-- Дамп данных таблицы `notes`
--

INSERT INTO `notes` (`id`, `IdRegion`, `idSubregion`, `typenotes`, `miniPhotoPath`, `Tinyinfo`, `infodata`, `idLogin`, `Name`, `email`, `telephone`, `sity`, `Status`, `photoPath1`, `photoPath2`, `photoPath3`, `photoPath4`, `date`) VALUES
(15, 3, 3, 1, 'smallImage_.jpeg', 'Десу', 'Десу весь де', 0, 'Дмитрий', '1', '123456789_123456789', 'Симферополь', 1, 'noteImage__1.jpeg', 'noteImage__2.jpeg', '', '', '2014-02-07'),
(16, 6, 5, 1, 'smallImage_15.jpeg', 'Севастополь - центр', '1', 0, '1', '4', '3', '23', 1, '', '', '', '', '2014-02-07'),
(17, 3, 3, 1, 'smallImage_16.jpeg', 'Новое в Джанкой', '12', 0, '1', '4', '3', '2', 1, '', '', '', '', '2014-02-07'),
(18, 3, 2, 1, 'smallImage_17.png', 'Привет Вася', 'Васи передайте привет', 1, 'Максим', 'playlinsor@gmail.com', '', '', 1, '', '', '', '', '2014-02-07'),
(19, 3, 3, 1, 'smallImage_18.jpeg', 'Привет от спящего коте', 'Коте спящий ЫЧ))', 3, 'Дарья', 'playlinsor@gmail.com', '', 'Красноярск', 1, 'noteImage_18_1.jpeg', '', '', '', '2014-02-09'),
(20, 6, 4, 2, 'smallImage_19.jpeg', 'Привет из портового района', 'Ыч))', 6, '', 'playlinsor@gmail.com', '', '', 1, 'noteImage_19_1.jpeg', '', '', '', '2014-02-11'),
(21, 1, 1, 2, '', 'Lorem ipsum dolor sit amet, elit clita scribentur no quo. Alia fabellas ei mei, platonem persecuti', 'Lorem ipsum dolor sit amet, elit clita scribentur no quo. Alia fabellas ei mei, platonem persecuti per at. Dicta nusquam adipiscing ei usu, et qui exerci pertinacia. Vocibus postulant deterruisset qui ei. Facete liberavisse suscipiantur in pro. Ceteros ponderum molestiae duo ad.\r\n\r\nQui vidit conceptam adversarium te, id ius mundi populo fastidii. Autem eirmod nam ei. Id est labores facilisis, ad summo tamquam reprimique vel, adhuc maiorum ius ei. Ridens ancjkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk', 0, 'Lorem ipsum dolor sit amet, el', 'Lorem ipsum dolor sit amet, el', 'Lorem ipsum dolor sit amet, el', 'Lorem ipsum dolor sit amet, elit clita scribentur', 1, '', '', '', '', '2014-02-12'),
(22, 1, 1, 2, '', 'Lorem ipsum dolor sit amet, ullum nobis nominati mea te. In vel reque fugit scriptorem, alii omnes v', 'Lorem ipsum dolor sit amet, ullum nobis nominati mea te. In vel reque fugit scriptorem, alii omnes vix no. Eu vero mutat splendide vel, est te laboramus delicatissimi vituperatoribus. Pri no alii imperdiet, qui libris insolens mediocrem eu. Iracundia tincidunt ei vel, has te suas reque ludus.\r\n\r\nSed equidem vituperata suscipiantur ad, cibo paulo id per, inani mucius persequeris id pro. At mei eligendi intellegat. Debitis mentitum democritum cu nec, populo nusquam tractatos an quo, ignota copiosa', 0, 'Lorem ipsum dolor sit amet, ullum nobis nominati m', 'Lorem ipsum dolor sit amet, ullum nobis nominati m', 'Lorem ipsum dolor sit amet, ullum nobis nominati m', 'Lorem ipsum dolor sit amet, ullum nobis nominati m', 1, '', '', '', '', '2014-02-12');

-- --------------------------------------------------------

--
-- Структура таблицы `regions`
--

CREATE TABLE IF NOT EXISTS `regions` (
  `id` int(11) NOT NULL,
  `nameRegion` varchar(40) NOT NULL,
  `position` int(11) NOT NULL,
  `count_notes` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица с основными регионамми';

--
-- Дамп данных таблицы `regions`
--

INSERT INTO `regions` (`id`, `nameRegion`, `position`, `count_notes`) VALUES
(1, 'Киевский', 1, 2),
(3, 'Крымский', 2, 4),
(5, 'Донецкий', 4, 0),
(6, 'Севастопольский', 3, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `subregions`
--

CREATE TABLE IF NOT EXISTS `subregions` (
  `id` int(11) NOT NULL,
  `nameRegion` varchar(40) NOT NULL,
  `mainRegion` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `count_notes` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Дополнительные регионы';

--
-- Дамп данных таблицы `subregions`
--

INSERT INTO `subregions` (`id`, `nameRegion`, `mainRegion`, `position`, `count_notes`) VALUES
(1, 'Новокиев', 1, 1, 2),
(2, 'Зуя', 3, 1, 1),
(3, 'Джанкой', 3, 2, 3),
(4, 'Портовый', 6, 2, 1),
(5, 'Центр', 6, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `temp_confirm_info`
--

CREATE TABLE IF NOT EXISTS `temp_confirm_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) NOT NULL,
  `comfirmhash` varchar(50) NOT NULL,
  `expirientsdate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Таблица где сохраняется информация о ключах подтверждения с емайлов' AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `temp_confirm_info`
--

INSERT INTO `temp_confirm_info` (`id`, `userId`, `comfirmhash`, `expirientsdate`) VALUES
(1, 6, '34ea5c9e07d48f1e4d515832c477f821', '2014-01-21');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  `name` varchar(50) NOT NULL,
  `first name` varchar(50) NOT NULL,
  `last name` varchar(50) NOT NULL,
  `gender` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `Sity` varchar(50) NOT NULL,
  `Organization` varchar(50) NOT NULL,
  `Telephone` varchar(50) NOT NULL,
  `info` varchar(400) NOT NULL,
  `birthday` varchar(11) NOT NULL,
  `avatar` varchar(60) NOT NULL,
  `access` int(11) NOT NULL COMMENT '10 - главный админ,5-модератор, 1- пользователь, 0 - забаненный',
  `confirmemail` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Таблица с данными о пользователях' AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `name`, `first name`, `last name`, `gender`, `email`, `Sity`, `Organization`, `Telephone`, `info`, `birthday`, `avatar`, `access`, `confirmemail`) VALUES
(1, 'Admin', 'e10adc3949ba59abbe56e057f20f883e', 'Дмитрий', 'Наумко', 'Владимирович', 1, 'admin@gmail.com', 'Симферополь', 'КРАБ 07', '', 'Админ, блин))', '07.06.1991', '', 1, 1),
(2, 'Dimon', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 1, 'Dimon@gmail.com', '', '', '', '', '', '', 3, 1),
(3, 'Wanna', 'e10adc3949ba59abbe56e057f20f883e', 'Дарья', 'Зайцева', '', 2, 'playlinsor@gmail.com', 'Красноярск', '', '', 'Художница', '01.01.1995', '', 3, 1),
(4, 'User', 'e10adc3949ba59abbe56e057f20f883e', 'Обычный пользователь', '-', '-', 1, 'user@gmai.com', '', '', '+380 67 144-33-44', 'Проверочный пользователь', '', '', 4, 0),
(5, 'Гость', 'нет', 'Гость', '', '', 0, '', '', '', '', '', '', '', 0, 0),
(6, 'PlayLinsor', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 1, 'playlinsor@gmail.com', '', '', '', '', '', '', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `users_roles`
--

CREATE TABLE IF NOT EXISTS `users_roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nameRole` varchar(30) NOT NULL,
  `access` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `users_roles`
--

INSERT INTO `users_roles` (`id`, `nameRole`, `access`) VALUES
(1, 'Администратор', 10),
(2, 'Модератор', 5),
(3, 'Пользователь', 3),
(4, 'Забанненый', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `user_ban`
--

CREATE TABLE IF NOT EXISTS `user_ban` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdUser` int(11) NOT NULL,
  `textBan` varchar(50) NOT NULL,
  `data` date NOT NULL,
  `dataRaz` date NOT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Заблокированные пользователи' AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `user_ban`
--

INSERT INTO `user_ban` (`id`, `IdUser`, `textBan`, `data`, `dataRaz`) VALUES
(1, 4, 'Рассылка спама', '2014-02-09', '2114-02-09');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
