<?php
/*	Вывод отображения и всякая системная мелочь
 * 
 * 
 */
ini_set('display_errors', 1);
session_start();

require_once 'Config.php';
require_once 'application/core/DataBaseFunction.php';
require_once 'application/controllers/FrontController.php';
require_once 'application/core/ConfigLang.php';
require_once 'application/core/TemplateMenuBuilder.php';

require_once 'application/core/_DebugInfo.php';


$front = FrontController::getInstans();
$front->route();

$DB = DataBaseFunction::getInstans();
$DB->CheckAndEchoError();

$Site = TemplateMenuBuilder::getInstans();
$Site->ShowBildedSite();

if (_DebugInfo::IsUse()) _DebugInfo::EndDebugging();
?>